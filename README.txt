This repository is a sample Ionic 4 app for an on screen logger.
The logger is on by default and must be turned off/on manually.

All `console.log`, `console.warn`, and `console.error` will be intercepted and displayed on screen (as well as in the console)
and all angular errors will be completely intercepted and displayed on screen (no longer in the terminal).

To integrate this into your project there are two steps.

Copy the file: `src/app/components/screen-logger/screen-logger.component.ts`
and add it anywhere in your project.

Next in app.module.ts add the following

```
@NgModule({
    declarations: [...],
    entryComponents: [...],
    imports: [...,
    providers: [
        ...,
        {provide: ErrorHandler, useClass: ScreenLoggerComponent}
    ],
    bootstrap: [...]
})
export class AppModule {}
```

And thats it! now all `console.log`, `console.warn`, and `console.error` will be intercepted and displayed on screen (as well as in the console)
and all angular errors will be completely intercepted and displayed on screen (no longer in the terminal).

`HTTP` based errors are red
`Type` errors are red
All other loggings are yellow. 

To disable it (useful for running `ionic serve`), simply comment out this line
`{provide: ErrorHandler, useClass: ScreenLoggerComponent}`


For ionic 3, go into `screen-logger.component.ts` and go to line `50`
Change the element id used to
`const elem = document.getElementById('nav');`